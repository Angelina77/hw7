# hw7



## Install Qdrant

using docker
    
```bash
docker run -p 6333:6333 -p 6334:6334 \
-e QDRANT__SERVICE__GRPC_PORT="6334" \
qdrant/qdrant
```
-p 6333:6333 maps port 6333 on the host to port 6333 in the container. This is typically the port that Qdrant's REST API listens on.

-p 6334:6334 maps port 6334 on the host to port 6334 in the container. This is used for Qdrant's gRPC interface.

QDRANT__SERVICE__GRPC_PORT="6334" specifies the port number that the Qdrant service should use for its gRPC API. By setting this environment variable, you're configuring Qdrant to listen on port 6334 for gRPC requests.

![image](qdrant.png)

## rust code
- Asynchronous Qdrant Client Setup: Utilizes tokio and qdrant_client to asynchronously interact with a Qdrant database server, ensuring efficient I/O operations.

- Collection Management: Dynamically creates a collection named my_collection, pre-configured to store 4-dimensional vectors and utilize the cosine distance metric for similarity comparisons.

- Data Insertion: Demonstrates the insertion of multiple data points into the Qdrant collection. Each data point comprises a 4-dimensional vector and a payload containing a name and an age attribute.

- Similarity Search Query: Executes a search query to find vectors similar to a given query vector within the collection, retrieving the top 5 closest vectors and their payloads.

- Console Visualization: Outputs the payload of each found point to the console, serving as a basic form of result visualization.
## build rust project

```bash
cargo run
```

## Visualization of the data
![image](result.png)



