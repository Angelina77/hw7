import json
import numpy as np
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt

# Load the vectors and payloads from the JSON file produced by the Rust program
with open('search_results.json', 'r') as file:
    data = json.load(file)

# Extract the vectors and their corresponding labels (payloads)
vectors = [item['vector'] for item in data]
labels = [item['payload']['name'] for item in data]  # Assuming payload contains 'name'

# Include the query vector; replace this with the actual query vector used in the Rust program
query_vector = [0.15, 0.25, 0.35, 0.45]

# Add the query vector to the list of vectors for PCA
all_vectors = [query_vector] + vectors

# Convert to numpy array for PCA
all_vectors_np = np.array(all_vectors)

# Apply PCA to reduce dimensions to 2D for visualization
pca = PCA(n_components=2)
reduced_vectors = pca.fit_transform(all_vectors_np)

# Plot the reduced vectors
plt.figure(figsize=(10, 6))

# Plot the search result vectors in blue
plt.scatter(reduced_vectors[1:, 0], reduced_vectors[1:, 1], color='blue', s=100, label='Search Results')

# Plot the query vector in red
plt.scatter(reduced_vectors[0, 0], reduced_vectors[0, 1], color='red', s=100, label='Query Vector')

# Annotating vectors with labels
for i, label in enumerate(['Query'] + labels):
    plt.annotate(label, (reduced_vectors[i, 0], reduced_vectors[i, 1]), textcoords="offset points", xytext=(0,10), ha='center')

plt.legend()
plt.xlabel('PCA Dimension 1')
plt.ylabel('PCA Dimension 2')
plt.title('PCA Visualization of Query and Search Result Vectors')
plt.grid(True)
plt.show()
