use anyhow::{Result, anyhow};
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    CreateCollection, SearchPoints, VectorParams, VectorsConfig, Distance,
};
use serde_json::json;
use qdrant_client::qdrant::vectors_config::Config;
use tokio;

#[tokio::main]
async fn main() -> Result<()> {
    // Initialize Qdrant client with the provided URL
    let client = QdrantClient::from_url("http://localhost:6334").build()?;

    // Define the name for the collection to be created or managed
    let collection_name = "my_collection";
    // Attempt to delete an existing collection if it exists, ignore any error
    client.delete_collection(collection_name).await.ok();

    // Create a new collection with specified configuration
    client.create_collection(&CreateCollection {
        collection_name: collection_name.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4, // Specify the size (dimension) of vectors to be stored
                distance: Distance::Cosine.into(), // Set the distance metric for vector comparison
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    // Insert multiple points with associated payloads into the database
    let vectors_to_insert = vec![
        json!({"vector": [0.2, 0.1, 0.9, 0.7], "payload": {"name": "Zichun Wang", "age": 25}}),
        json!({"vector": [0.1, 0.2, 0.8, 0.6], "payload": {"name": "Kevin Tian", "age": 22}}),
        json!({"vector": [0.9, 0.8, 0.1, 0.3], "payload": {"name": "Camilia Guo", "age": 35}}),
        json!({"vector": [0.4, 0.4, 0.5, 0.5], "payload": {"name": "Andy Tai", "age": 40}}),
        json!({"vector": [0.3, 0.3, 0.6, 0.4], "payload": {"name": "Lily Zhang", "age": 30}}),
        json!({"vector": [0.5, 0.5, 0.4, 0.6], "payload": {"name": "Tommy Li", "age": 45}}),
        json!({"vector": [0.6, 0.7, 0.3, 0.2], "payload": {"name": "Maggie Wang", "age": 28}}),
        json!({"vector": [0.7, 0.6, 0.2, 0.1], "payload": {"name": "Linda Zhang", "age": 32}}),
        json!({"vector": [0.8, 0.9, 0.1, 0.1], "payload": {"name": "Jacky Li", "age": 38}}),
        json!({"vector": [0.9, 0.1, 0.7, 0.8], "payload": {"name": "Cathy Tai", "age": 42}}),
    ];

    for (i, item) in vectors_to_insert.into_iter().enumerate() {
        let vector: Vec<f32> = item["vector"].as_array().unwrap().iter().map(|v| v.as_f64().unwrap() as f32).collect();
        // Convert the payload from serde_json::Value to the expected format and handle conversion errors
        let payload = item["payload"].clone().try_into().map_err(|e| anyhow!("Payload conversion error: {:?}", e))?;

        // Create points with the vector and payload, then insert them into the specified collection
        let points = vec![PointStruct::new(i as u64, vector, payload)];
        client.upsert_points(collection_name, None, points, None).await?;
    }

    // Perform a search within the collection for vectors similar to the query vector
    let search_result = client.search_points(&SearchPoints {
        collection_name: collection_name.into(),
        vector: vec![0.15, 0.25, 0.35, 0.45], // Query vector
        filter: None,
        limit: 5, // Number of results to return
        with_payload: Some(true.into()), // Include payloads in the search results
        ..Default::default()
    }).await?;

    // Visualization: Display the payload of each found point
    for (index, point) in search_result.result.iter().enumerate() {
        println!("Point {} Payload: {:?}", index + 1, point.payload);
    }

    Ok(())
}
